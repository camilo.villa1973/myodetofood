﻿using MyOdeToFood.Core;
using System.Linq;
using System.Collections.Generic;

namespace MyOdeToFood.Data
{
    public interface IRestaurantData
    {
        IEnumerable<Restaurant> GetRestaurantsByName(string name);
        Restaurant GetById(int restaurantId);
        Restaurant Update(Restaurant updatedRestaurant);
        Restaurant Add(Restaurant newRestaurant);
        int Commit();
    }

    public class InMemoryRestaurantData : IRestaurantData
    {
        List<Restaurant> restaurants;
        public InMemoryRestaurantData()
        {
            restaurants = new List<Restaurant>
            {
                new Restaurant{Id=1, Name="La Fragatta", Location="Laureles", Cuisine=CuisineType.Italian},
                new Restaurant{Id=2, Name="Las Costillas de Pedro", Location="Poblado", Cuisine=CuisineType.Mexican},
                new Restaurant{Id=3, Name="Popeyanas Cuisine", Location="Belén", Cuisine=CuisineType.Indian}
            };
        }
        public IEnumerable<Restaurant> GetRestaurantsByName(string name = null) => from r in restaurants where string.IsNullOrEmpty(name) || r.Name.ToLower().StartsWith(name.ToLower()) orderby r.Name select r;
        public Restaurant GetById(int restaurantId) => restaurants.SingleOrDefault(restaurant=>restaurant.Id == restaurantId);

        public Restaurant Update(Restaurant updatedRestaurant)
        {
            var current = restaurants.SingleOrDefault(restaurant => restaurant.Id == updatedRestaurant.Id);
            if(current != null)
            {
                current.Name = updatedRestaurant.Name;
                current.Location = updatedRestaurant.Location;
                current.Cuisine = updatedRestaurant.Cuisine;
            }

            return current;
        }

        public Restaurant Add(Restaurant newRestaurant)
        {
            newRestaurant.Id = restaurants.Max(x => x.Id) + 1;
            restaurants.Add(newRestaurant);

            return newRestaurant;
        }

        public int Commit()
        {
            return 0;
        }
    }
}
